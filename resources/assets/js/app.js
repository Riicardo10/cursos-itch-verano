require('./bootstrap');

window.Vue = require('vue');

Vue.component('general', require('./components/General.vue'));
Vue.component('area-vue', require('./components/Area.vue'));
Vue.component('carrera-vue', require('./components/Carrera.vue'));
Vue.component('semestre-vue', require('./components/Semestre.vue'));
Vue.component('materia-vue', require('./components/Materia.vue'));
Vue.component('rol-vue', require('./components/Rol.vue'));
Vue.component('profesor-vue', require('./components/Profesor.vue'));
Vue.component('usuario-vue', require('./components/Usuario.vue'));
Vue.component('plan-estudio-vue', require('./components/PlanEstudio.vue'));
Vue.component('materias-solicitadas-por-coordinador-vue', require('./components/MateriasSolicitadasCoordinador.vue'));
Vue.component('materias-solicitadas-por-jefe-vue', require('./components/MateriasSolicitadasJefe.vue'));
Vue.component('estudiante-vue', require('./components/Estudiante.vue'));

const app = new Vue({
    el: '#app',
    data: {
        menu: 0,
    },
    methods: {
        passData:function(){
            return self.menu
        }
    }
});
