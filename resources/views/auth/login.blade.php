@extends('auth.contenido')

@section('login')
    <div class="col-md-1"></div>
    <div class="col-md-4">
        <form method="POST" style="width=100%" action="/">
            @csrf
            <table width="100%">
                <tr>
                    <td colspan="2">
                        <center>
                            <img src="/images/default.png" width='40%' style="margin-bottom:5%">
                        </center>
                    </td>
                    <!--<td colspan="2"><h3 style="text-align:center; color:#345;"><i>::: INICIO DE SESIÓN :::</i></h3></td>-->
                </tr>
                <tr>
                    <td>    <font size="3"> Email: </font>  </td>
                    <td>    <input type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>    </td>
                </tr>
                <tr>
                    <td>    <font size="3"> Password: </font>   </td>
                    <td>    <input type="password" class="form-control" name="password" required>   </td>
                </tr>
                <tr>
                    <td></td>
                    <td>    {!! $errors->first('email', "<span style='color:#B95353;'> :message </span>") !!}    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button type="submit" class="btn btn-success">  Iniciar   </button>
                        <a class="btn btn-link" href="/password/reset">    Olvidaste tu contraseña?    </a>
                    </td>
                </tr>
            </table>
            
        </form>
    </div>
    <div class="col-md-1"></div>
@endsection