<!DOCTYPE html>
<html>
	<head>
		<title>Inicio</title>
		<meta charset='utf-8'>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link href="/css/app-all.css" rel="stylesheet">
		<style>
			.fondo{
				width: 100%;
				height: 450px;
				opacity: 0.7;
			}
		</style>
	</head>
	<body style='background: #fff'>
		<div class='container' style='background: rgba(5,4,5,0);' id="app">
			<div class="row" style="background:white">
				<div class="col-12">
					<img src="/images/logo-tec.jpg" width=100%' height='auto'>
				</div>
			</div>
			<div class="row">
				
				<div class="col-12" style="background: #248;">
					@if( Auth::check() )
						@if( Auth::user()->id_rol == 1 )
							<h2 class="titulo" style="color:white; padding-top:1%; padding-bottom: 1%; text-align: center">Bienvenido Administrador</h2>
						@endif
						@if( Auth::user()->id_rol == 2 )
							<h2 class="titulo" style="color:white; padding-top:1%; padding-bottom: 1%; text-align: center">Bienvenido Coordinador</h2>
						@endif
						@if( Auth::user()->id_rol == 3 )
							<h2 class="titulo" style="color:white; padding-top:1%; padding-bottom: 1%; text-align: center">Bienvenido Jefe de departamento</h2>
						@endif
					@endif
				</div>
			</div>
			<div class="row" >
				<div class="container">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						@if( Auth::check() )
							<!--	ADMIN	-->
							@if( Auth::user()->id_rol == 1 )
								<li @click="menu=0" class="nav-item">
									<a class="nav-link" href="#">General</a>
								</li>
								<li @click="menu=2" class="nav-item">
									<a class="nav-link" href="#">Areas</a>
								</li>
								<li @click="menu=3" class="nav-item">
									<a class="nav-link" href="#">Carreras</a>
								</li>
								<li @click="menu=4" class="nav-item">
									<a class="nav-link" href="#">Semestres</a>
								</li>
								<li @click="menu=5" class="nav-item">
									<a class="nav-link" href="#">Materias</a>
								</li>
								<li @click="menu=6" class="nav-item">
									<a class="nav-link" href="#">Roles</a>
								</li>
								<li @click="menu=7" class="nav-item">
									<a class="nav-link" href="#">Profesores</a>
								</li>
								<li @click="menu=13" class="nav-item">
									<a class="nav-link" href="#">Estudiantes</a>
								</li>
								<li @click="menu=8" class="nav-item">
									<a class="nav-link" href="#">Usuarios</a>
								</li>	
							@endif
							<!--	COORDINADOR	-->
							@if( Auth::user()->id_rol == 2 )
								<li @click="menu=0" class="nav-item">
									<a class="nav-link" href="#">General</a>
								</li>
								<li @click="menu=9" class="nav-item">
									<a class="nav-link" href="#">Plan de estudio</a>
								</li>
								<li @click="menu=11" class="nav-item">
									<a class="nav-link" href="#">Materias solicitadas</a>
								</li>
							@endif
							<!--	JEFE	-->
							@if( Auth::user()->id_rol == 3 )
								<li @click="menu=0" class="nav-item">
									<a class="nav-link" href="#">General</a>
								</li>
								<li @click="menu=12" class="nav-item">
									<a class="nav-link" href="#">Materias solicitadas</a>
								</li>
							@endif
							<!--	CERRAR SESION	-->
							<form action="/logout" id="logout" style="display: none" method="POST">
								@csrf
							</form>
							<li class="nav-item">
								<a class="nav-link" href="/logout" onclick="event.preventDefault(); document.getElementById('logout').submit();"> Cerrar sesión de {{Auth::user()->email}} </a>
							</li>
						@endif
					</ul>
				</div>
			</div>
			<!--	TAB-PANES	-->
			<!--	CONTENEDORES VUE	-->
			<div class="row" >
				<div class="col-md-12">
					<template v-if="menu==0">
						<general></general>
					</template>
					<template v-if="menu==2">
						<area-vue></area-vue>
					</template>
					<template v-if="menu==3">
						<carrera-vue></carrera-vue>
					</template>
					<template v-if="menu==4">
						<semestre-vue></semestre-vue>
					</template>
					<template v-if="menu==5">
						<materia-vue></materia-vue>
					</template>
					<template v-if="menu==6">
						<rol-vue></rol-vue>
					</template>
					<template v-if="menu==7">
						<profesor-vue></profesor-vue>
					</template>
					<template v-if="menu==8">
						<usuario-vue></usuario-vue>
					</template>
					<template v-if="menu==9">
						<plan-estudio-vue></plan-estudio-vue>
					</template>
					<template v-if="menu==10">
						<rol-vue></rol-vue>
					</template>
					<template v-if="menu==11">
						<materias-solicitadas-por-coordinador-vue></materias-solicitadas-por-coordinador-vue>
					</template>
					<template v-if="menu==12">
						<materias-solicitadas-por-jefe-vue></materias-solicitadas-por-jefe-vue>
					</template>
					<template v-if="menu==13">
						<estudiante-vue></estudiante-vue>
					</template>
				</div>
			</div>
		</div>
	</body>

	<script src="/js/app.js"></script>
    <script src="/js/app-all.js"></script>

</html>