<?php

// INVITADO
Route::group( [ 'middleware'=>['guest'] ], function( ) {
    Route::get( '/', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post( '/', 'Auth\LoginController@login');
    Route::get('/lista_materia/{id}', 'ListaMateriaController@show');
} );

Route::group( [ 'middleware'=>['auth'] ], function( ) {

    // PARA TODOS
    Route::get('/principal', function () {
        return view('principal');    
    });
    Route::post( '/logout', 'Auth\LoginController@logout');

    // ADMIN
    Route::group( [ 'middleware'=>['Admin'] ], function( ) {
        Route::resource('/rol', 'RolController');
        Route::get('/roles', 'RolController@getRoles');
        Route::put( '/rol/activar/id', 'RolController@activar' );
        Route::put( '/rol/desactivar/id', 'RolController@desactivar' );
        Route::resource('/usuario', 'UserController');
        Route::get('/usuario-rol', 'UserController@getUsuarioByRol');
        Route::put( '/usuario/activar/id', 'UserController@activar' );
        Route::put( '/usuario/desactivar/id', 'UserController@desactivar' );    
        Route::resource('/estudiante', 'EstudianteController');
    } );

    // COORDINADOR
    Route::group( [ 'middleware'=>['Coordinador'] ], function( ) {
        Route::resource('/materia', 'MateriaController');
        Route::get('/curriculum/{id}', 'CurriculumController@curriculum');
        Route::get('/curriculum_semestre/{id}', 'CurriculumController@curriculum_semestre');
        Route::get('/estudiantes/{matricula}', 'EstudianteController@getEstudiante');
        Route::get('/materias_semestre/{id_carrera}/{id_semestre}', 'CurriculumController@getMateriasPorSemestre');
        Route::resource('/materia_solicitada', 'MateriaSolicitadaController');
        Route::get('/cantidad-materias', 'MateriaSolicitadaController@getCantidadMateriasSolicitadas');
        Route::get('/materias_solicitadas', 'MateriaSolicitadaController@getMateriasSolicitadas');
        Route::resource('/lista_materia', 'ListaMateriaController');
        // Route::get('/lista_materia/{id}', 'ListaMateriaController@getListadoMateriaSolicitada');
        Route::get('/bandera_listado/{id}', 'ListaMateriaController@getBanderaMateriaSolicitada');
    } );

    // JEFE_DEPARTAMENTO
    Route::group( [ 'middleware'=>['JefeDepartamento'] ], function( ) {
        Route::resource('/area', 'AreaController');
        Route::get('/areas', 'AreaController@getAreas');
        Route::resource('/carrera', 'CarreraController');
        Route::get('/carreras', 'CarreraController@getCarreras');
        Route::resource('/semestre', 'SemestreController');
        Route::get('/semestres', 'SemestreController@getSemestres');
        Route::resource('/materia', 'MateriaController');
        Route::resource('/profesor', 'ProfesorController');
        Route::get('/profesores', 'ProfesorController@getProfesores');
        Route::put( '/profesor/activar/clave', 'ProfesorController@activar' );
        Route::put( '/profesor/desactivar/clave', 'ProfesorController@desactivar' );
        Route::get('/materias_solicitadas_jefe', 'MateriaSolicitadaController@getMateriasSolicitadasJefeDepartamento');
        Route::put( '/materias_solicitadas_jefe/aprobar/id', 'MateriaSolicitadaController@aprobarMateria' );
        Route::put( '/materias_solicitadas_jefe/no_aprobar/id', 'MateriaSolicitadaController@rechazarMateria' );
        Route::get('/materias_solicitadas/pdf/{id}', 'MateriaSolicitadaController@getMateriasSolicitadasPDF');
    } );

} );



Route::get('test', function() {
    dd(Config::get('mail'));
});



/*      AUTH       */
 Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');
