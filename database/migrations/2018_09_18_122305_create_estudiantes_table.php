<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiantesTable extends Migration {
    
    public function up() {
        Schema::create('estudiantes', function (Blueprint $table) {
            $table->string('matricula', 8);
            $table->primary('matricula');
            $table->string('nombre', 150);
            $table->string('email', 360);
            $table->string('telefono', 20);
            $table->integer('id_carrera')->unsigned();
            $table->foreign('id_carrera')->references('id')->on('carreras');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('estudiantes');
    }
}
