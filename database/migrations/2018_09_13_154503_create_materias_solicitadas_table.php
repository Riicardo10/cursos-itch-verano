<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriasSolicitadasTable extends Migration {
    
    public function up() {
        Schema::create('materias_solicitadas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_coordinador')->unsigned();
            $table->foreign('id_coordinador')->references('id')->on('users');
            $table->string('clave_profesor', 40)->nullable();
            $table->foreign('clave_profesor')->references('clave')->on('profesores');
            $table->string('clave_materia', 40);
            $table->foreign('clave_materia')->references('clave')->on('materias');
            $table->boolean('aprobada')->default(0);
            $table->string('anio', 4);
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('materias_solicitadas');
    }
}
