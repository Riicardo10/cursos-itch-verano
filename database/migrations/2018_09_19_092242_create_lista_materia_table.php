<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListaMateriaTable extends Migration {
    
    public function up() {
        Schema::create('lista_materia', function (Blueprint $table) {
            $table->integer('id_materia')->unsigned();
            $table->string('clave_materia', 40);
            $table->string('matricula', 8);
            $table->foreign('id_materia')->references('id')->on('materias_solicitadas');
            $table->foreign('clave_materia')->references('clave')->on('materias');
            $table->foreign('matricula')->references('matricula')->on('estudiantes');
            
            $table->primary( [
                'id_materia', 
                'clave_materia',
                'matricula'
            ] );
            //$table->foreign('id_materia')->references('id')->on('materias_solicitadas');
        });
    }

    public function down() {
        Schema::dropIfExists('lista_materia');
    }
}
