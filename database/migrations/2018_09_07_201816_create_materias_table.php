<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMateriasTable extends Migration {
    public function up() {
        Schema::create('materias', function (Blueprint $table) {
            $table->string('clave', 40);
            $table->primary('clave');
            $table->string('materia', 100);
            $table->integer('creditos');
            $table->integer('id_carrera')->unsigned();
            $table->foreign('id_carrera')->references('id')->on('carreras');
            $table->integer('id_semestre')->unsigned();
            $table->foreign('id_semestre')->references('id')->on('semestres');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('materias');
    }
}
