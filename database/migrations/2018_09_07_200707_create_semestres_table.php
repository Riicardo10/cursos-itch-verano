<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSemestresTable extends Migration {
    public function up() {
        Schema::create('semestres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('semestre', 50);
            $table->timestamps();
        });
    }
    public function down() {
        Schema::dropIfExists('semestres');
    }
}
