<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration {
    public function up() {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rol', 30)->unique();
            $table->string('descripcion', 100)->nullable();
            $table->boolean('estado')->default(1);
            $table->timestamps();
        });
        DB::table('roles')->insert(array('id'=>1, 'rol'=>'admin', 'descripcion'=>'Administrador'));
        DB::table('roles')->insert(array('id'=>2, 'rol'=>'coordinador', 'descripcion'=>'Coordinador de materias'));
        DB::table('roles')->insert(array('id'=>3, 'rol'=>'subdirector', 'descripcion'=>'Subdirector académico'));    
        DB::table('roles')->insert(array('id'=>4, 'rol'=>'jefe', 'descripcion'=>'Jefe de departamento'));    
    }
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
