<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesoresTable extends Migration {
    public function up() {
        Schema::create('profesores', function (Blueprint $table) {
            $table->string('clave', 40);
            $table->primary('clave');
            $table->string('nombre', 150);
            $table->string('email', 150);
            $table->string('telefono', 15);
            $table->boolean('estado')->default(1);
            $table->integer('id_area')->unsigned();
            $table->foreign('id_area')->references('id')->on('areas');
            $table->timestamps();

        });
    }

    public function down() {
        Schema::dropIfExists('profesores');
    }
}
