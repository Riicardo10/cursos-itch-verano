<?php

return [

    'driver' => env('MAIL_DRIVER', 'smtp'),

    //'host' => env('MAIL_HOST', 'smtp.mailgun.org'),
    //'host' => env('MAIL_HOST', 'smtp.mailtrap.io'),
    'host' => env('MAIL_HOST', 'smtp.gmail.com'),

    //'port' => env('MAIL_PORT', 587),
    'port' => env('MAIL_PORT', 2525),

    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'ricardobugs9@gmail.com'),
        'name' => env('MAIL_FROM_NAME', 'Ricardo Flores'),
    ],

    'encryption' => env('MAIL_ENCRYPTION', 'tls'),

    'username' => env('MAIL_USERNAME'),
    'password' => env('MAIL_PASSWORD'),

    'sendmail' => '/usr/sbin/sendmail -bs',

    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],

];
