<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ListaMateria extends Model {
    protected $table = 'lista_materia';
    public $timestamps = false;
}
