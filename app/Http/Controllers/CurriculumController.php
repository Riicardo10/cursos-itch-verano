<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Materia;
use App\Semestre;

class CurriculumController extends Controller {
    
    public function curriculum(Request $request, $id){
        $curriculum = Materia::join('carreras', 'materias.id_carrera', '=','carreras.id')
            ->join('semestres', 'materias.id_semestre','=','semestres.id')
            ->select('materias.clave','materias.materia','materias.creditos','carreras.id as id_carrera','carreras.carrera as carrera','semestres.id as id_semestre','semestres.semestre')
            ->where('carreras.id','=', $id)
            ->orderBy('materias.id_semestre', 'asc')->get();
        $semestres = Semestre::count();
        return [ 
            'curriculum' => $curriculum,
            'semestres' => $semestres
        ];
    }

    public function curriculum_semestre(Request $request, $id) {
        $i = 0;
        $curriculum = array();
        $semestres = Materia::select('id_semestre')->groupBy('id_semestre')->where('id_carrera','=', $id)->orderBy('id_semestre', 'asc')->get();
        foreach($semestres as $semestre) {
            $materias = Materia::select('clave','materia','creditos', 'id_semestre')->where('id_carrera','=', $id)->where('id_semestre','=', $semestre->id_semestre)->orderBy('id_semestre', 'asc')->get();
            $temp = array();
            $j = 0;
            foreach($materias as $materia) {
                $temp[$j] = $materia;
                $j++;
            }
            $curriculum[$i] = $temp;
            $i++;
        }
        return [ 
            'curriculum' => $curriculum,
            'semestres' => $semestres
        ];
    }

    public function getMateriasPorSemestre(Request $request, $id_carrera, $id_semestre) {
        $materias = Materia::select('clave','materia','creditos', 'id_semestre')
        ->where('id_carrera','=', $id_carrera)
        ->where('id_semestre','=', $id_semestre)
        ->orderBy('id_semestre', 'asc')->get();
        return [ 'materias' => $materias ];
    }

}
