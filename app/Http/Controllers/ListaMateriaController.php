<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ListaMateria;

class ListaMateriaController extends Controller {

    public function show(Request $request, $id) {
        $listado = ListaMateria::join('materias_solicitadas', 'lista_materia.id_materia', '=', 'materias_solicitadas.id')
            ->join('estudiantes', 'estudiantes.matricula', '=', 'lista_materia.matricula')
            ->leftJoin('carreras', 'estudiantes.id_carrera', '=', 'carreras.id')
            ->select('estudiantes.matricula as estudiante_matricula', 'estudiantes.nombre as estudiante_nombre',
            'estudiantes.email as estudiante_email', 'estudiantes.telefono as estudiante_telefono',
            'carreras.carrera as estudiante_carrera' )
            ->where('lista_materia.id_materia', '=', $id)
            ->orderBy('lista_materia.id_materia', 'desc')->get();
        return [ 'listado' => $listado ];
    }
    
    public function index( Request $request ) {
        $listado = ListaMateria::join('materias_solicitadas', 'lista_materia.id_materia', '=', 'materias_solicitadas.id')
            ->leftJoin('profesores', 'materias_solicitadas.clave_profesor', '=', 'profesores.clave')
            ->leftJoin('areas', 'profesores.id_area', '=', 'areas.id')
            ->leftJoin('users', 'users.id', '=', 'materias_solicitadas.id_coordinador')
            ->join('materias', 'materias.clave', '=', 'materias_solicitadas.clave_materia')
            ->join('estudiantes', 'estudiantes.matricula', '=', 'lista_materia.matricula')
            ->leftJoin('carreras', 'estudiantes.id_carrera', '=', 'carreras.id')
            ->select(   'lista_materia.id_materia as listado_id', 'lista_materia.clave_materia as listado_clave', 
            'lista_materia.matricula as listado_matricula', 
            'profesores.clave as profesor_clave', 'profesores.nombre as profesor_nombre', 
            'profesores.email as profesor_email','areas.area as profesor_area',
            'users.name as coordinador_nombre', 'users.email as coordinador_email',
            'materias.clave as materia_clave', 'materias.materia as materia_nombre',
            'materias.creditos as materia_creditos', 'materias.id_semestre as materia_semestre',
            'estudiantes.matricula as estudiante_matricula', 'estudiantes.nombre as estudiante_nombre',
            'estudiantes.email as estudiante_email', 'estudiantes.telefono as estudiante_telefono',
            'carreras.carrera as estudiante_carrera' )
            ->orderBy('lista_materia.id_materia', 'desc')->get();
        return [ 'listado' => $listado ];
    }

    public function store(Request $request) {
        for($i=0; $i<count($request->lista_estudiantes); $i++) {
            $listado = new ListaMateria();
            $listado->id_materia = $request->id_materia;
            $listado->clave_materia = $request->clave_materia;
            $listado->matricula = $request->lista_estudiantes[$i]['matricula'];
            $listado->save();
        }
    }

    public function getBanderaMateriaSolicitada(Request $request, $id_materia_solicitada) {
        $bandera = ListaMateria::where('id_materia', '=', $id_materia_solicitada)
        ->count('id_materia');
        return [ 'bandera' => $bandera ];
    }

}