<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Carrera;

class CarreraController extends Controller {
    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $carreras = Carrera::orderBy('updated_at', 'desc')->paginate(5);
        else
            $carreras = Carrera::where('carrera', 'like', '%' . $request->buscar . '%')->orderBy('updated_at', 'desc')->paginate(5);
        return [
            'paginacion' => [
                'total' => $carreras->total(),
                'pagina_actual' => $carreras->currentPage(),
                'por_pagina' => $carreras->perPage(),
                'ultima_pagina' => $carreras->lastPage(),
                'desde' => $carreras->firstItem(),
                'hasta' => $carreras->lastItem()
            ],
            'carreras' => $carreras
        ];
    }

    public function store(Request $request) {
        $carrera = new Carrera();
        $carrera->carrera = $request->carrera;
        $carrera->save();
    }

    public function update(Request $request, $id) {
        $carrera = Carrera::findOrFail( $request->id );
        $carrera->carrera = $request->carrera;
        $carrera->save();   
    }

    public function getCarreras() {
        $carreras = Carrera::orderBy('carrera', 'asc')->get();
        return [ 'carreras' => $carreras ];
    }
}
