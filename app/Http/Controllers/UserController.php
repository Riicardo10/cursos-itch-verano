<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller {

    public function index( Request $request ) {
        if( $request->buscar == '' ) 
            $usuarios = User::join('roles', 'users.id_rol', '=','roles.id')
            ->select(   'users.id','users.name','users.email','users.telefono','users.direccion', 'users.password',
                        'users.email','users.estado','roles.id as id_rol', 'roles.rol', 'roles.estado as estado_rol')
            ->orderBy('name', 'asc')->paginate(5);
        else
            $usuarios = User::join('roles', 'users.id_rol', '=','roles.id')
            ->select(   'users.id','users.name','users.email','users.telefono','users.direccion', 'users.password',
                        'users.email','users.estado','roles.id as id_rol', 'roles.rol', 'roles.estado as estado_rol')
            ->where('users.name', 'like', '%' . $request->buscar . '%')
            ->orderBy('name', 'asc')->paginate(5);
        return [
            'paginacion' => [
                'total' => $usuarios->total(),
                'pagina_actual' => $usuarios->currentPage(),
                'por_pagina' => $usuarios->perPage(),
                'ultima_pagina' => $usuarios->lastPage(),
                'desde' => $usuarios->firstItem(),
                'hasta' => $usuarios->lastItem()
            ],
            'usuarios' => $usuarios
        ];
    }

    public function store(Request $request) {
        $usuario = new User();
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);
        $usuario->direccion = $request->direccion;
        $usuario->telefono = $request->telefono;
        $usuario->id_rol = $request->id_rol;
        $usuario->save();
    }

    public function update(Request $request, $id) {
        $usuario = User::findOrFail( $id );
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->direccion = $request->direccion;
        $usuario->telefono = $request->telefono;
        $usuario->id_rol = $request->id_rol;
        $usuario->save();   
    }

    public function desactivar(Request $request) {
        $user = User::findOrFail( $request->id );
        $user->estado = '0';
        $user->save();
    }

    public function activar(Request $request) {
        $user = User::findOrFail( $request->id );
        $user->estado = '1';
        $user->save();
    }
    public function getUsuarioByRol(Request $request) {
        
        $usuarios = User::join('roles', 'users.id_rol', '=','roles.id')
        ->select(   'users.id','users.name','users.email','users.telefono','users.direccion', 'users.password',
                    'users.email','users.estado','roles.id as id_rol', 'roles.rol', 'roles.estado as estado_rol')
        ->where('users.id_rol', '=', $request->id_rol)
        ->orderBy('users.name', 'asc')->paginate(5);
        return [
            'paginacion' => [
                'total' => $usuarios->total(),
                'pagina_actual' => $usuarios->currentPage(),
                'por_pagina' => $usuarios->perPage(),
                'ultima_pagina' => $usuarios->lastPage(),
                'desde' => $usuarios->firstItem(),
                'hasta' => $usuarios->lastItem()
            ],
            'usuarios' => $usuarios
        ];
    }

}
